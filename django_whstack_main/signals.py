from django.db.models.signals import post_save
from django.dispatch import receiver

from django_whstack_main.django_whstack_main.tasks import whstack_default_vars
from django_whstack_models.django_whstack_models.models import WhWebsites


@receiver(post_save, sender=WhWebsites)
def run_task_whstack_default_vars(sender, instance, **kwargs):
    whstack_default_vars.apply()

from OpenSSL import crypto
import dns.resolver
from django.conf import settings

from django_powerdns_models.django_powerdns_models.models import Domains
from django_whstack_models.django_whstack_models.tasks import *


@shared_task(name="whstack_tenant_sync")
def whstack_tenant_sync():
    all_domain_customers = WhRegistryDomains.objects.values("customer").distinct()

    for domain_customer in all_domain_customers:
        try:
            tenant = WhTenantSettings.objects.get(
                customer__id=domain_customer["customer"]
            )
        except WhTenantSettings.DoesNotExist:
            tenant = WhTenantSettings(
                customer_id=domain_customer["customer"],
            )

        tenant.dns_enabled = True
        tenant.domains_enabled = True
        tenant.save()

    return


@shared_task(name="whstack_default_vars")
def whstack_default_vars():
    for website in WhWebsites.objects.all().select_related():
        try:
            acme_email = website.whwebsitevars_set.get(name="acme_email")
        except WhWebsiteVars.DoesNotExist:
            acme_email = WhWebsiteVars(
                name="acme_email", website=website, value="crtnoc@dolphin-it.de"
            )
            acme_email.save()

        try:
            tls_enable = website.whwebsitevars_set.get(name="tls_enable")
        except WhWebsiteVars.DoesNotExist:
            tls_enable = WhWebsiteVars(name="tls_enable", website=website, value="true")
            tls_enable.save()

        try:
            tls_force_redir = website.whwebsitevars_set.get(name="tls_force_redir")
        except WhWebsiteVars.DoesNotExist:
            tls_force_redir = WhWebsiteVars(
                name="tls_force_redir", website=website, value="true"
            )
            tls_force_redir.save()

        try:
            tls_renew = website.whwebsitevars_set.get(name="tls_renew")
        except WhWebsiteVars.DoesNotExist:
            tls_renew = WhWebsiteVars(name="tls_renew", website=website, value="true")
            tls_renew.save()

        try:
            tls_source = website.whwebsitevars_set.get(name="tls_source")
        except WhWebsiteVars.DoesNotExist:
            tls_source = WhWebsiteVars(
                name="tls_source", website=website, value="letsencrypt.org"
            )
            tls_source.save()

        try:
            tls_key_size = website.whwebsitevars_set.get(name="tls_key_size")
        except WhWebsiteVars.DoesNotExist:
            tls_key_size = WhWebsiteVars(
                name="tls_key_size", website=website, value="4096"
            )
            tls_key_size.save()

        try:
            tls_key_type = website.whwebsitevars_set.get(name="tls_key_type")
        except WhWebsiteVars.DoesNotExist:
            tls_key_type = WhWebsiteVars(
                name="tls_key_type", website=website, value="ecdsa"
            )
            tls_key_type.save()

        try:
            tls_elliptic_curve = website.whwebsitevars_set.get(
                name="tls_elliptic_curve"
            )
        except WhWebsiteVars.DoesNotExist:
            tls_elliptic_curve = WhWebsiteVars(
                name="tls_elliptic_curve", website=website, value="secp256r1"
            )
            tls_elliptic_curve.save()

        try:
            tls_cert = website.whwebsitevars_set.get(name="tls_cert")
            tls_key = website.whwebsitevars_set.get(name="tls_key")
        except WhWebsiteVars.DoesNotExist:
            # create a self-signed cert
            new_tls_key = crypto.PKey()
            new_tls_key.generate_key(crypto.TYPE_RSA, int(tls_key_size.value))

            new_tls_cert = crypto.X509()
            new_tls_cert.get_subject().CN = "example.com"
            new_tls_cert.gmtime_adj_notBefore(0)
            new_tls_cert.gmtime_adj_notAfter(10 * 365 * 24 * 60 * 60)
            new_tls_cert.set_issuer(new_tls_cert.get_subject())
            new_tls_cert.set_pubkey(new_tls_key)
            new_tls_cert.sign(new_tls_key, "sha512")

            tls_cert = WhWebsiteVars(
                name="tls_cert",
                website=website,
                value=crypto.dump_certificate(crypto.FILETYPE_PEM, new_tls_cert).decode(
                    "utf-8"
                ),
            )
            tls_cert.save()

            tls_key = WhWebsiteVars(
                name="tls_key",
                website=website,
                value=crypto.dump_privatekey(crypto.FILETYPE_PEM, new_tls_key).decode(
                    "utf-8"
                ),
            )
            tls_key.save()

    return


@shared_task(name="whstack_create_webs_for_domains")
def whstack_create_webs_for_domains():
    domain_list = list()

    domain_list.extend(
        list(WhRegistryDomains.objects.all().values_list("domain", flat=True))
    )
    domain_list.extend(
        list(
            Domains.objects.exclude(name__endswith=".arpa")
            .exclude(name__endswith=".local")
            .values_list("name", flat=True)
        )
    )

    # make sure there are no duplicates
    domain_list = list(set(domain_list))

    for domain in domain_list:
        # check if domain is connected to a web
        try:
            reg_domain = WhDomains.objects.get(domain=domain)
        except WhDomains.DoesNotExist:
            pass
        except WhDomains.MultipleObjectsReturned:
            continue
        else:
            continue

        # indentify cluster
        clusters = {
            "185.118.196.155": {
                "cluster_id": 1,
            },
            "185.118.196.154": {
                "cluster_id": 2,
            },
            "185.118.197.123": {
                "cluster_id": 3,
            },
            "185.118.197.130": {
                "cluster_id": 4,
            },
        }

        try:
            result = dns.resolver.resolve(domain, "A")
        except:
            print("Domain %s is invalid" % domain)
            continue

        if result[0].to_text() not in clusters.keys():
            continue

        # detect customer
        customer_id = settings.MDAT_ROOT_CUSTOMER_ID

        try:
            customer_id = WhRegistryDomains.objects.get(domain=domain).customer_id
        except WhRegistryDomains.DoesNotExist:
            for reg_check_domain in WhRegistryDomains.objects.all():
                if domain.endswith(reg_check_domain.domain):
                    customer_id = reg_check_domain.customer_id
                    continue

        # create web
        new_web = WhWebsites(
            web_name=domain,
            cluster_id=clusters[result[0].to_text()]["cluster_id"],
            customer_id=customer_id,
        )
        new_web.save(force_insert=True)

        new_domain = WhDomains(
            domain=domain,
            include_sub="WWW",
            website=new_web,
        )
        new_domain.save(force_insert=True)

    return


@shared_task(name="whstack_cleanup_old_webs")
def whstack_cleanup_old_webs():
    for domain in WhDomains.objects.filter(website__cluster_id__in=(1, 2, 3)):
        try:
            result = dns.resolver.resolve(domain.domain, "A")
        except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer):
            print("Domain %s is invalid" % domain.domain)
            domain.delete()
            continue

        ips = [
            "185.118.196.155",
            "185.118.196.154",
            "185.118.197.123",
        ]

        if result[0].to_text() in ips:
            continue

        print(f"{domain.domain} - IP: {result[0].to_text()}")
        domain.delete()

    for website in WhWebsites.objects.all():
        if not len(website.whdomains_set.all()):
            print(f"{website.web_name} has no domains")

            website.whwebsitevars_set.all().delete()
            website.delete()

    return

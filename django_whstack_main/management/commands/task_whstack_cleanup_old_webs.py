from django.core.management.base import BaseCommand

from django_whstack_main.django_whstack_main.tasks import whstack_cleanup_old_webs


class Command(BaseCommand):
    def handle(self, *args, **options):
        whstack_cleanup_old_webs()

from django.core.management.base import BaseCommand

from django_whstack_main.django_whstack_main.tasks import (
    whstack_create_webs_for_domains,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        whstack_create_webs_for_domains()

from django.core.management.base import BaseCommand

from django_whstack_main.django_whstack_main.tasks import whstack_default_vars


class Command(BaseCommand):
    help = "Run task whstack_default_vars"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        whstack_default_vars()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))

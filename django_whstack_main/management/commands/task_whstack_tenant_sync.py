from django.core.management.base import BaseCommand

from django_whstack_main.django_whstack_main.tasks import whstack_tenant_sync


class Command(BaseCommand):
    help = "Run task whstack_tenant_sync"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        whstack_tenant_sync()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))

from django.template import engines

from django_whstack_models.django_whstack_models.models import *


def get_web_vars(wh_web: WhWebsites):
    template_opts = dict()

    template_opts["site_id"] = wh_web.id
    template_opts["site_id_long"] = "site-id-" + str(wh_web.id)

    for variable in wh_web.whwebsitevars_set.all():
        template_opts[variable.name] = variable.value

    return template_opts


def get_dom_vars(wh_dom: WhDomains):
    domain_expanded = list()

    domain_expanded.append(wh_dom.domain)

    if wh_dom.include_sub == wh_dom.NONE:
        pass
    elif wh_dom.include_sub == wh_dom.WWW:
        domain_expanded.append("www." + wh_dom.domain)
    elif wh_dom.include_sub == wh_dom.WILDCARD:
        domain_expanded.append("*." + wh_dom.domain)
    else:
        pass

    return domain_expanded


def get_httpfront_nginx(wh_web: WhWebsites):
    django_engine = engines["django"]
    module_prefix = "django_cdstack_tpl_httpfront/django_cdstack_tpl_httpfront"

    all_domains = list()

    for domain in wh_web.whdomains_set.all():
        all_domains.extend(get_dom_vars(domain))

    special_opts = get_web_vars(wh_web)
    special_opts["vhost_urls"] = " ".join(all_domains)

    config_template_file = open(
        module_prefix
        + "/templates/config-fs/dynamic/etc/nginx/sites-available/site.conf",
        "r",
    ).read()
    config_template = django_engine.from_string(config_template_file)

    nginx_site_conf = config_template.render(special_opts)

    return nginx_site_conf


def get_pbxproxyweb_nginx(wh_web: WhWebsites):
    django_engine = engines["django"]
    module_prefix = "django_cdstack_tpl_pbxproxy_web/django_cdstack_tpl_pbxproxy_web"

    all_domains = list()

    for domain in wh_web.whdomains_set.all():
        all_domains.extend(get_dom_vars(domain))

    special_opts = get_web_vars(wh_web)
    special_opts["vhost_urls"] = " ".join(all_domains)

    config_template_file = open(
        module_prefix
        + "/templates/config-fs/dynamic/etc/nginx/sites-available/site.conf",
        "r",
    ).read()
    config_template = django_engine.from_string(config_template_file)

    nginx_site_conf = config_template.render(special_opts)

    return nginx_site_conf


def get_acmecert_certbot_config(wh_web: WhWebsites):
    django_engine = engines["django"]
    module_prefix = "django_cdstack_tpl_acmecert/django_cdstack_tpl_acmecert"

    all_domains = list()

    for domain in wh_web.whdomains_set.all():
        all_domains.extend(get_dom_vars(domain))

    special_opts = get_web_vars(wh_web)
    special_opts["domains"] = ",".join(all_domains)

    config_template_file = open(
        module_prefix
        + "/templates/config-fs/dynamic/etc/letsencrypt/config/le-site.conf",
        "r",
    ).read()
    config_template = django_engine.from_string(config_template_file)

    acmecert_certbot_site_conf = config_template.render(special_opts)

    return acmecert_certbot_site_conf
